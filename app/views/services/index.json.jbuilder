json.array!(@services) do |service|
  json.extract! service, :id, :namel, :name, :description, :category_id
  json.url service_url(service, format: :json)
end
