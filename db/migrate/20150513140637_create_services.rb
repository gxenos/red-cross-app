class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :namel
      t.string :name
      t.text :description
      t.string :category_id

      t.timestamps null: false
    end
  end
end
